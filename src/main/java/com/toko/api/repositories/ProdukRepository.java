package com.toko.api.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.toko.api.models.Produk;

@Repository
public interface ProdukRepository extends JpaRepository<Produk, Long> {

}
