package com.toko.api.models;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Table;

@Entity
@Table(name = "produks")
public class Produk {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@Column(nullable = false, length = 64)
	private String kodeProduk;
	
	@Column(nullable = false, length = 64)
	private String namaProduk;
	
	@Column(nullable = false, length = 32)
	private int hargaProduk;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getKodeProduk() {
		return kodeProduk;
	}

	public void setKodeProduk(String kodeProduk) {
		this.kodeProduk = kodeProduk;
	}

	public String getNamaProduk() {
		return namaProduk;
	}

	public void setNamaProduk(String namaProduk) {
		this.namaProduk = namaProduk;
	}

	public int getHargaProduk() {
		return hargaProduk;
	}

	public void setHargaProduk(int hargaProduk) {
		this.hargaProduk = hargaProduk;
	}
}
