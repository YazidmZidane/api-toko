package com.toko.api.controllers;

import java.util.HashMap;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.toko.api.models.Login;
import com.toko.api.models.User;
import com.toko.api.repositories.UserRepository;

@RestController
@RequestMapping("/api")
public class UserController {

	@Autowired
	UserRepository userRepository;

	@RequestMapping(value = "/register", method = RequestMethod.POST)
	public HashMap<String, Object> registrasi(@RequestBody User body) {
		HashMap<String, Object> result = new HashMap<String, Object>();

		User user = userRepository.save(body);

		result.put("Status", 200);
		result.put("Message", "Registrasi Berhasil !!");
		result.put("Data", user);

		return result;
	}

	@RequestMapping(value = "/login", method = RequestMethod.POST)
	public HashMap<String, Object> login(@RequestBody Login body) {
		HashMap<String, Object> result = new HashMap<String, Object>();

		User user = userRepository.findUserByEmail(body.getEmail());

		if (user != null) {
			if (user.getPassword().equals(body.getPassword())) {
				result.put("Status", 200);
				result.put("Message", "Login Berhasil !!");
				result.put("Data", user);
			} else {
				result.put("Status", 500);
				result.put("Message", "Login Gagal, Password tidak sesuai !!");
				result.put("Data", null);
			}
		} else {
			result.put("Status", 500);
			result.put("Message", "Login Gagal, Email tidak ditemukan !!");
			result.put("Data", null);
		}
		
		return result;
	}

	@RequestMapping(value = "/", method = RequestMethod.GET)
	public HashMap<String, Object> getDataUser() {
		HashMap<String, Object> result = new HashMap<String, Object>();

		List<User> users = userRepository.findAll();

		result.put("Status", 200);
		result.put("Message", "Registrasi Berhasil !!");
		result.put("Data", users);

		return result;
	}
}
