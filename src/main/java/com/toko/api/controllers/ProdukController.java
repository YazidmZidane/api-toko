package com.toko.api.controllers;

import java.util.HashMap;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.toko.api.models.Produk;
import com.toko.api.repositories.ProdukRepository;

@RestController
@RequestMapping("/api")
public class ProdukController {

	@Autowired
	ProdukRepository produkRepository;

	@RequestMapping(value = "/produks", method = RequestMethod.GET)
	public HashMap<String, Object> getAllProduk() {
		HashMap<String, Object> result = new HashMap<String, Object>();

		List<Produk> produks = produkRepository.findAll();

		result.put("Status", 200);
		result.put("Message", "Get All Data Produk Berhasil !!");
		result.put("Data", produks);

		return result;
	}

	@RequestMapping(value = "/produk", method = RequestMethod.POST)
	public HashMap<String, Object> createNewProduk(@RequestBody Produk body) {
		HashMap<String, Object> result = new HashMap<String, Object>();

		Produk produk = produkRepository.save(body);

		result.put("Status", 200);
		result.put("Message", "Berhasil Menambahkan Data Baru !!");
		result.put("Data", produk);

		return result;
	}

	@RequestMapping(value = "/produk/{id}/update", method = RequestMethod.POST)
	public HashMap<String, Object> updateProduk(@PathVariable(value = "id") Long id, @RequestBody Produk body) {
		HashMap<String, Object> result = new HashMap<String, Object>();
		
		Produk entity = produkRepository.findById(id).orElseThrow(() -> null);
		
		entity.setKodeProduk(body.getKodeProduk());
		entity.setNamaProduk(body.getNamaProduk());
		entity.setHargaProduk(body.getHargaProduk());

		Produk produk = produkRepository.save(entity);

		result.put("Status", 200);
		result.put("Message", "Berhasil Mengubah Data Produk !!");
		result.put("Data", true);

		return result;
	}
	
	@RequestMapping(value = "/produk/{id}", method = RequestMethod.GET)
	public HashMap<String, Object> getOneProduk(@PathVariable(value = "id") Long id) {
		HashMap<String, Object> result = new HashMap<String, Object>();

		Produk produk = produkRepository.findById(id).orElseThrow(() -> null);

		result.put("Status", 200);
		result.put("Message", "Berhasil Menampilkan Data Produk !!");
		result.put("Data", produk);

		return result;
	}
	
	@RequestMapping(value = "/produk/{id}", method = RequestMethod.DELETE)
	public HashMap<String, Object> deleteProduk(@PathVariable(value = "id") Long id) {
		HashMap<String, Object> result = new HashMap<String, Object>();

		Produk produk = produkRepository.findById(id).orElseThrow(() -> null);
		
		produkRepository.delete(produk);

		result.put("Status", 200);
		result.put("Message", "Berhasil Menampilkan Data Produk !!");
		result.put("Data", true);

		return result;
	}
}
